var BASE = {
    /**
     * @description api请求基础路径
     */
    API_DEV: {
      common: "http://localhost:8890",
      buyer: "http://localhost:8888",
      seller: "http://localhost:8889",
      manager: "http://localhost:8887"
    },
    API_PROD: {
      common: "http://192.168.2.110:8890",
      buyer: "http://192.168.2.110:8888",
      seller: "http://192.168.2.110:8889",
      manager: "http://192.168.2.110:8887"
    },
  };
